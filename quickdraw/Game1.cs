﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // Needed for SFX
using Microsoft.Xna.Framework.Media; // Needed for music

namespace quickdraw
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D buttonTexture;
        SpriteFont gameFont;


        // game state / input 

        bool playing = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            buttonTexture = Content.Load<Texture2D>("button");
            gameFont = Content.Load<SpriteFont>("MainSpriteFont");
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            MouseState currentstate = Mouse.GetState();

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here
            spriteBatch.Begin();

            //find the centre of the screen 
            Vector2 screencentre = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);


            spriteBatch.Draw(buttonTexture, new Rectangle((int)screencentre.X - buttonTexture.Width / 2, (int)screencentre.Y - buttonTexture.Height / 2, buttonTexture.Width, buttonTexture.Height), Color.Green);

            Vector2 titlesize = gameFont.MeasureString("Quick Draw!");
            Vector2 titlesize2 = gameFont.MeasureString("By Gregor Anderson");

            spriteBatch.DrawString(gameFont, "Quick Draw! ", screencentre - new Vector2(0, 100) - titlesize / 2, Color.White);
            spriteBatch.DrawString(gameFont, "By Gregor Anderson ", screencentre - new Vector2(0, 75) - titlesize2 / 2, Color.White);


            spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
